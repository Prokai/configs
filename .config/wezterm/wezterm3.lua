return {
  color_scheme = 'carbonfox',
  default_cursor_style = 'SteadyBlock',
  enable_tab_bar = false,
  window_background_opacity = 1,
  window_close_confirmation = "NeverPrompt",
  check_for_updates = false,
}
