return {
  color_scheme = 'Atom',
  default_cursor_style = 'SteadyUnderline',
  default_prog = { '/bin/zsh' },
  enable_tab_bar = false,
  window_background_opacity = 0.8,
  window_close_confirmation = "NeverPrompt",
}
